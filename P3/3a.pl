%% :- use_module(library(lists)).

%% postfix using append:

%% postfix(X, Y):-
%%     append(_, X, Y). % whatever + X = Y ?

%% postfix not using append:

prefix(X, Y):-
    append(X, _, Y).

postfix(X, Y):-
    reverse(Y, ReversedY),
    reverse(X, ReversedX),
    prefix(ReversedX, ReversedY).
%% postfix([], []). % stop condition: both lists ended here



%% postfix([Head | SubTail], [Head | MainTail]):- % one atom match, move on the the next atoms
%%     postfix(SubTail, MainTail).

%% postfix([SubHead | SubTail], [MainHead | MainTail]):- % one atom does't match, retry with the next one on main list
%%     SubHead \= MainHead,
%%     postfix([SubHead | SubTail], MainTail).