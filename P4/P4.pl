%% grammar for lists end with "01" NEA: N = (Z, sigma, delta, Z0, E).
%% define states
state(z0).
state(z1).
state(z2_end).

%% define alphabet (sigma set)
alphabet(0).
alphabet(1).

%% conditions for transitional relations, exit branch otherwise, for example when z = z1, input = 0
delta_f(z0, 0, z0).
delta_f(z0, 1, z0).
delta_f(z0, 0, z1).
delta_f(z1, 1, z2_end).

%% start states:
start(z0).

%% end states:
end(z2_end).

%% relations:
%% generated words set (sigma*): each character in this word must exist in the base alphabet set
sigma_star([]).
sigma_star([Atom|Rest]):-
    alphabet(Atom),
    sigma_star(Rest).

%% transitional relations:
delta_set(Z_current, [], Z_current). %% stop condition, reached the end of the list
delta_set(Z_current, [Atom|Rest], Z_last):-
    %% start(Z_current), %% z must be eligible to start a transition
    delta_f(Z_current, Atom, Z_temp), %% check this transition with z and atom
    delta_set(Z_temp, Rest, Z_last). %% check other atoms in list
    

l_of_n(List):-
    sigma_star(List), %% firstly, all atoms must be valid
    start(Z_start),
    delta_set(Z_start, List, Z_last),
    state(Z_last),
    end(Z_last).
